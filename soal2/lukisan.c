#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <signal.h>
 
// Fungsi untuk membuat program killer
void createKiller(int mode) {
    int status = 0;
    char* process_name = (mode == 1) ? "download_process" : "folder_process";
 
    // Generate c code for killer program
    FILE* fp = fopen("killer.c", "w+");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <unistd.h>\n\nint main() {\n");
    fprintf(fp, "\tsystem(\"pkill -f -e -c %s\");\n", process_name);
    fprintf(fp, "\treturn 0;\n}\n");
    fclose(fp);
 
    // Compile and remove c file
    pid_t killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    } else {
        waitpid(killer_pid, &status, 0);
    }

    pid_t rm_pid = fork();
    if(rm_pid == 0) {
        execl("/usr/bin/rm", "rm", "killer.c", NULL);
    } else {
        waitpid(rm_pid, &status, 0);
    }
}
 
// Fungsi utama
int main(int argc, char* argv[]) {
    // Check program mode
    if(argc != 2 || (strcmp("-a", argv[1]) != 0 && strcmp("-b", argv[1]) != 0)) {
        printf("PILIH MODE A dengan -a atau MODE B dengan -b\n");
        exit(1);
    }
    if (strcmp("-a",argv[1]) == 0) {
        printf("Mode A\n");
        createKiller(1);
    } else {
        printf("Mode B\n");
        createKiller(2);
    }

    // Pid list
    __pid_t pid;
    __pid_t folder_pid;
    __pid_t child_pid;
    __pid_t zip_pid;
 
    int zipStatus = 0;
 
    // Download link
    char link[100];
    time_t epoch;
    struct tm *cur_time;
 
    // Folder and file names
    char datetime[20];
    char foldertime[20];
    char outputFile[100];
    char *path = "//mnt/c/users/hasan/documents/sisop/mod2/no2rev";
 
    // Make folder creator into a daemon
    pid = fork(); // membuat process fork pertama
    if (pid < 0) {
        printf("Error in creating a daemon!\n");
        exit(1);
    }
    if (pid > 0) {
        printf("\n");
        exit(0);
    }




    umask(0); //// mengubah umask
    pid = setsid(); // Mengatur permission umask ke 0 dan membuat session baru dengan menggunakan setsid().

    // Folder downloaders
    while(1) {      //Melakukan loop tak terbatas dengan while(1), kemudian melakukan fork() dan membuat child process baru yang akan melakukan tugas tertentu.
        folder_pid = fork();
        sprintf(argv[0], "folder_process");
        if(folder_pid == 0) {

            time(&epoch);       //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel foldertime. Membuat direktori baru dengan menggunakan mkdir() dengan permission 0777.
            cur_time = localtime(&epoch);
            strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time);
            mkdir(foldertime, 0777);

            // Image downloaders
            for(int j = 0; j < 15; j++) { //Melakukan loop sebanyak 15 kali untuk membuat 15 child process baru yang akan mendownload gambar.
                child_pid = fork();
                sprintf(argv[0], "download_process");

                if(child_pid == 0) {        //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan chdir() untuk berpindah ke direktori yang ditentukan. Mengatur link URL yang akan didownload dan menyimpannya dalam variabel link. Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel datetime. Menggunakan sprintf() untuk mengatur format output file dan menyimpannya dalam variabel outputFile. Menggunakan execl() untuk menjalankan perintah wget untuk mendownload gambar. Setelah selesai, child process akan keluar dari program menggunakan exit(0).
                    time(&epoch);
                    cur_time = localtime(&epoch);
                    chdir(path);
                    sprintf(link, "https://p...content-available-to-author-only...m.photos/%ld", epoch%1000 + 50);
                    strftime(datetime, 100, "%Y-%m-%d_%X", cur_time);
                    sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime);
                    execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL);
                    exit(0);
                }
                sleep(5); //Menunda selama 5 detik menggunakan sleep().
            }
            //ZIP
            // Melakukan fork() untuk membuat child process baru yang akan melakukan kompresi terhadap direktori yang telah dibuat sebelumnya. Menggunakan execl() untuk menjalankan perintah zip untuk melakukan kompresi. Setelah kompresi selesai, menggunakan waitpid() untuk menunggu child process selesai. Menggunakan execl() lagi untuk menjalankan perintah rm untuk menghapus direktori yang telah dikompresi. Setelah selesai, child process akan keluar dari program menggunakan exit(0).
            zip_pid = fork();
            if(zip_pid == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL);
            }   
            waitpid(zip_pid, &zipStatus, 0);
            execl("/usr/bin/rm", "rm -r ", foldertime, NULL);
            exit(0);
        }
        sleep(30); // Menunda selama 30 detik menggunakan sleep() 
    }
    return 0;
}
 
 

