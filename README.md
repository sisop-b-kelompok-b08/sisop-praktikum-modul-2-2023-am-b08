# Praktikum Modul 2 Sistem Operasi 
Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211225 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan soal nomor 1

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <regex.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

int main(){

    /*
     * Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. 
     * Dan untuk melakukan melihat file gambar pada folder yang telah didownload 
     * Grape-kun harus melakukan unzip pada folder tersebut
     */

    // Download animal image files from Google Drive
    download_file("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");

    // Unzip animal image files to 'binatang' directory
    unzip_animal("binatang.zip");

    /*
     * Setelah berhasil melakukan unzip 
     * Grape-kun melakukan pemilihan secara acak pada file gambar tersebut 
     * untuk melakukan shift penjagaan pada hewan tersebut
     */

    // Select a random animal image file from the 'binatang' directory
    get_random_file(".");

    /*
     * Karena Grape-kun adalah orang yang perfeksionis 
     * Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. 
     * Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. 
     * Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan 
     * sesuai dengan tempat tinggal nya.
     */

    // Create directories 'HewanDarat', 'HewanAmphibi', 'HewanAir' to store animal images
    make_directory();

    // Move animal images to their respective directories based on their habitat
    filter_animal(".", "HewanDarat", "HewanAmphibi", "HewanAir");

    /*
     * Setelah mengetahui hewan apa saja yang harus dijaga 
     * Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan
     */

    // Zip directories 'HewanDarat', 'HewanAmphibi', 'HewanAir' to save space
    zip_animal();

    return 0;
}
```
### Penjelasan header yang digunakan
Ada beberapa header file yang digunakan untuk mengakses berbagai fungsi dari sistem operasi dan fungsi bawaan C. Berikut penjelasan singkat dari setiap header file tersebut:
- ``stdio.h``: menyediakan fungsi-fungsi input-output standar seperti ``printf()`` dan ``scanf()``.
- ``stdlib.h``: menyediakan fungsi-fungsi umum seperti ``malloc()`` dan ``free()``.
- ``unistd.h``: menyediakan akses ke API sistem operasi seperti ``fork()`` dan ``exec()``.
- ``time.h``: menyediakan fungsi-fungsi terkait waktu seperti ``time()`` dan ``localtime()``.
- ``pwd.h``: menyediakan fungsi-fungsi untuk mengakses informasi akun pengguna seperti ``getpwuid()``.
- ``regex.h``: menyediakan fungsi-fungsi untuk melakukan pencocokan pola (pattern matching) dengan menggunakan regular expression.
- ``dirent.h``: menyediakan fungsi-fungsi untuk mengakses direktori dan file di dalamnya seperti ``opendir()`` dan ``readdir()``.
- ``string.h``: menyediakan fungsi-fungsi terkait manipulasi string seperti ``strcpy()`` dan ``strlen()``.
- ``sys/types.h``: menyediakan definisi tipe data seperti ``pid_t`` dan ``mode_t``.
- ``sys/stat.h``: menyediakan fungsi-fungsi untuk memeriksa atribut file seperti ``stat()`` dan ``chmod()``.
- ``sys/wait.h``: menyediakan fungsi-fungsi terkait dengan penggunaan proses anak seperti ``wait()`` dan ``waitpid()``.

### Penjelasan tiap-tiap fungsi

Fungsi ``main`` memiliki 6 fungsi yang berperan penting. Berikut adalah langkah-langkah dari fungsi yang tersedia:

### Penjelasan fungsi ``download_file()``
Pengunduhan gambar melalui Google Drive diperlukan, sehingga kami membuat fungsi yang mengunduh ``binatang.zip`` dari link Google Drive yang disediakan. Berikut adalah fungsi dari ``void download_file``:
```
/**
 * Downloads a file from the given URL and saves it as the given file name using the 'wget' utility.
 *
 * @param url - The URL of the file to be downloaded.
 * @param file_animal - The name of the file to be saved.
 */
void download_file(char* url, char* file_animal) {

  // Fork a new process to run 'wget' utility in the child process.
  pid_t pid = fork();

  // Check if the current process is the child process.
  if (pid == 0) {
    
    // In the child process, execute the 'wget' command with the given arguments.
    execlp("wget", "wget", "--no-check-certificate", url, "-q", "-O", file_animal, (char*) NULL);
    
    // Exit the child process if the 'execlp' command fails.
    exit(1);
  }

  // Wait for the child process to complete.
  int status;
  waitpid(pid, &status, 0);
}
```
Fungsi tersebut berguna untuk mengunduh sebuah file dari url yang nantinya ditentukan yaitu ``https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq`` dan menyimpannya dengan nama file yang diberikan. Fungsi tersebut juga menggunakan ``wget`` sehingga pengunduhan dapat dilaksanakan.

Fungsi tersebut menerima dua paramater yaitu ``char* url, char* file_animal`` yang merupakan alamat URL dari file yang akan diunduh dan ``file_animal`` yang merupakan nama file yang akan disimpan pada penyimpanan lokal komputer.

- Pertama, fungsi akan melakukan ``fork`` untuk membuat proses aanak yang akan menjalankan utilitas dari ``wget``.
- Kedua, pada proses anak, fungsi ``execlp`` akan dieksekusi dengan argumen ``wget`` dan parameter-parameter yang diberikan. Fungsi ini akan menjalankan ``wget`` dengan argumen yang sesuai untuk mengunduh file dari URL yang telah diberikan dan menyimpannya dengan nama file yang telah diberikan.

Setelah proses ``wget`` selesai dijalankan, proses anak akan keluar dan proses utama akan menunggu proses anak selesai dengan menggunakan ``waitpid``.

Berikut adalah implementasi dari fungsi ``main``:
```
download_file("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
```
Kode fungsi tersebut memanggil ``download_file`` yang akan mengunduh sebuah file dari URL tertentu yaitu ``https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq`` dan menyimpannya sebagai ``binatang.zip`` pada penyimpanan lokal komputer.

### Penjelasan fungsi ``unzip_animal()``
Setelah ``binatang.zip`` diunduh, file  tersebut perlu diunzip agar kita bisa mengakses isi dari file tersebut. Berikut adalah fungsi dari ``void unzip_animal``:
```
/**
 * Unzips a given ZIP file using the 'unzip' utility.
 *
 * @param animal_zip - The name of the ZIP file to be unzipped.
 */
void unzip_animal(char* animal_zip) {

  // Fork a new process to run 'unzip' utility in the child process.
  pid_t pid = fork();

  // Check if the current process is the child process.
  if (pid == 0) {
    
    // In the child process, execute the 'unzip' command with the given arguments.
    execlp("unzip", "unzip", "-q", animal_zip, (char*) NULL);
    
    // Exit the child process if the 'execlp' command fails.
    exit(1);
  }

  // Wait for the child process to complete.
  int status;
  waitpid(pid, &status, 0);
}
```
Fungsi ini digunakan untuk mengekstrak file ZIP dengan menggunakan utilitas ``unzip``. Fungsi tersebut menerima satu parameter, yaitu ``animal_zip`` yang merupakan nama file ZIP yang akan diekstrak.
- Pertama, fungsi ini menggunakan ``fork`` untuk membuat sebuah proses baru.
- Kedua, pada kondisi percabangan ``if``, apabila proses saat ini merupakan proses anak, fungsi ``execlp`` akan dieksekusi dengan argumen ``unzip`` yang akan dijalankan dengan argumen yang sesuai untuk mengekstrak file ZIP.

Jika perintah ``unzip`` gagal dijalankan, maka program akan keluar dari proses anak menggunakan ``exit``. Kemudian, pada akhir fungsi, program akan menunggu hingga proses anak selesai menggunakan ``waitpid``.

Secara keseluruhan, fungsi tersebut berguna untuk mengekstrak file ZIP dengan utilitas ``unzip`` melalui pemanggilan fungsi ``void unzip_animal`` dengan memberikan nama file ZIP yang akan diekstrak sebagai parameter.

Berikut adalah implementasi dari fungsi ``main``:
```
    unzip_animal("binatang.zip");
```
Kode fungsi tersebut bertujuan untuk melakukan proses ekstraksi file ``binatang.zip`` yang telah diunduh sebelumnya ke dalam sebuah folder bernama ``binatang``. Setelah folder ``binatang`` dibuat dan file ZIP diunzip, file-file gambar hewan yang terdapat dalam ZIP file tersebut akan tersedia untuk diproses selanjutnya.

## Penjelasan fungsi ``get_random_file()``
Setelah file diunzip, pengambilan gambar acak dari file dilakukan lalu dituliskan di txt file yaitu ``penjaga.txt``. Berikut adalah fungsi dari ``void get_random_file``:

```
/**
 * Selects a random image file in the given directory with the extension '.jpg', 
 * and writes its filename to a new text file named 'penjaga.txt'.
 *
 * @param directory - The path to the directory containing the image files.
 */
void get_random_file(char *directory)
{
  // Open the directory for reading.
  DIR *folder = opendir(directory);
  if (!folder) return;

  // Read the filenames of image files in the directory and store them in the 'path' array.
  struct dirent *dp;
  char path[100][256];  // Array to store filenames
  int count = 0;

  while ((dp = readdir(folder)) && count < 100)
  {
    if (dp->d_type == DT_REG && strstr(dp->d_name, ".jpg"))
      strcpy(path[count++], dp->d_name);
  }

  // Close the directory.
  closedir(folder);

  // If no image files were found in the directory, return without creating the text file.
  if (!count) return;

  // Generate a random number within the range of the 'path' array to select a random image file.
  srand(time(NULL));
  int random = rand() % count;

  // Create a new text file 'penjaga.txt' and write the name of the selected image file to it.
  FILE *file = fopen("penjaga.txt", "w");
  if (!file) return;

  fprintf(file, "Hewan yang dijaga : %.*s", (int)(strrchr(path[random], '.') - path[random]), path[random]);
  fclose(file);
}
```
Fungsi ``void get_random_file`` memiliki tujuan yaitu memilih sebuah file gambar secara acak dengan ekstensi ``.jpg`` dari direktori yang telah diberikan dan menuliskan nama file gambar yang telah dipilih ke dalam file txt baru bernama ``penjaga.txt``.

- Pertama, fungsi akan membuka direktori yang tersedia dan membaca nama file di dalamanya dengan menggunakan ``opendir`` dan ``readdir``. File-file yang memiliki ekstensi ``.jpg`` akan disimpan dalam array ``path``. Kemudian, jika file tidak ditemukan, fungsi akan mengembalikan nilai kosong dan keluar dari fungsi.
- Kedua, fungsi akan menghasilan sebuah nomor acar dari fungsi ``rand()`` yang disesuaikan dengan waktu saat ini. Nomor acak tersebut akan digunakan sebagai indeks array ``path`` untuk memilih file gambar secara acak.
- Ketiga, fungsi akan membuat file txt baru dengan nama ``penjaga.txt`` menggunakan ``fopen``. Kemudian, akan menuliskan nama file gambar yang dipilih ke dalam file teks dengan format ``"Hewan yang dijaga : nama_file.jpg"`` menggunakan ``fprintf``. Setelah selesai menuliskkan, file teks akan ditutup menggunakan ``fclose``.

Berikut adalah implementasi dari fungsi ``main``:
```
    get_random_file(".");
```
Kode fungsi akan memilih file gambar hewan secara dengan ekstensi ``.jpg`` dari direktori saat ini dan menuliskan nama file yang dipilih ke dalam sebuah file teks baru yang bernama ``penjaga.txt``.

## Penjelasan fungsi ``make_directory()``
Setelah dipilih secara random ke dalam ``penjaga.txt``, hewan-hewan tersebut akan dibuatkan direktori terpisah yang dibagi menjadi tiga yaitu ``HewanDarat, HewanAmphibi, dan HewanAir`` lalu nanti dimasukan dengan masing-masing kategori. Berikut adalah implementasi pembuatan direktori masing-masing kategorid dari fungsi ``void make_directory``:
```
/**
 * Creates three directories named HewanDarat, HewanAir, and HewanAmphibi if they do not exist yet.
 * Uses fork() to create child processes for each directory creation to run in parallel.
 */
void make_directory() {
  // Array of directory names to create
  const char* dirs[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
  pid_t child_id;
  int stat;
  // Loop through each directory name and create a child process for each using fork()
  for (int i = 0; i < sizeof(dirs) / sizeof(*dirs); i++) {
    if ((child_id = fork()) == 0) {
      // Child process calls mkdir command with -p flag to create the directory and its parent directories if they do not exist
      execlp("mkdir", "mkdir", "-p", dirs[i], NULL);
      // Exit child process
      exit(0);
    }
    // Wait for child process to finish before moving to the next directory
    waitpid(child_id, &stat, 0);
  }
}
```
Fungsi tersebut berguna untuk membuat tiga direktori baru dengan nama ``HewanDarat, HewanAmphibi, dan HewanAir``jika ketiga direktori tersebut belum ada. Fungsi tersebut menggunakan ``fork`` untuk membuat proses anak untuk setiap pembuatan direktori, sehingga proses pembuatan direktori dapat berjalan secara paralel.

- Pertama, fungsi ``make_directory`` mendefinisikan array ``dirs`` yang berisi nama-nama direktori yang ingin dibuat. Kemudian, dalam ``for loop``, fungsi tersebut melakukan iterasi pada setiap elemen dari array ``dirs`` dan membuat proses anak untuk setiap iterasi.
- Kedua, dalam setiap iterasi, proses anak dibuat dengan memanggil ``fork``. Proses anak kemudian memanggil perintah ``mkdir`` menggunakan ``execlp`` dengan menggunakan argumen ``-p`` dan nama direktori yang ingin dibuat. Flag ``-p`` menunjukkan kepada ``mkdir`` untuk membuat direktori bersama dengan direktori induknya jika direktori induknya belum ada. Setelah proses anak selesai membuat direktori, ia keluar menggunakan perintah ``exit``.
- Ketiga, setelah proses anak selesai, proses induk menunggu proses anak selesai dengan menggunakan ``waitpid``. Setelah proses anak selesai, proses induk akan melanjutkan iterasi berikutnya pada array ``dirs``. Setelah semua iterasi selesai dilaksanakaan, fungsi ``make_directory`` selesai dan direktori-direktori baru sudah dibuat jika belum ada sebelumnya.

Berikut adalah implementasi dari fungsi ``main``:
```
make_directory();
```
Fungsi ``make_directory`` dipanggil untuk membuat direktori dengan kategori masing-masing yaitu ``HewanDarat, HewanAmphibi, dan HewanAir``.

## Penjelasan fungsi ``filter_animal``
Setelah direktori terbuat, saaatnya file-file gambar difilter sesuai dengan kategori masing-masing yaitu ``HewanDarat, HewanAmphibi, dan HewanAir``. Berikut adalah fungsi dari ``void filter_animal``:
```
/**
 * This function filters animal images into three different categories
 * based on the string "darat", "air", or "amphibi" in their filename,
 * and moves them to the corresponding directories.
 *
 * @param source The source directory containing the animal images
 * @param dest1 The destination directory for animals categorized as "darat"
 * @param dest2 The destination directory for animals categorized as "air"
 * @param dest3 The destination directory for animals categorized as "amphibi"
 */
void filter_animal(char *source, char *dest1, char *dest2, char *dest3)
{
  struct dirent *dp;
  DIR *folder;
  pid_t pid;
  
  // Open the source directory
  folder = opendir(source);
  if (folder == NULL)
    return;
  
  // Traverse all files in the source directory
  while ((dp = readdir(folder)) != NULL)
  {
    // Ignore non-regular files
    if (dp->d_type != DT_REG)
      continue;
      
    char *dest = NULL;
    // Determine the category of the animal image
    if (strstr(dp->d_name, "darat") != NULL)
      dest = dest1;
    else if (strstr(dp->d_name, "amphibi") != NULL)
      dest = dest2;
    else if (strstr(dp->d_name, "air") != NULL)
      dest = dest3;
    else
      continue;
      
    // Fork a child process to move the file to the corresponding directory
    pid = fork();
    if (pid == 0)
    {
      char src_path[PATH_MAX];
      char dest_path[PATH_MAX];
      snprintf(src_path, PATH_MAX, "%s/%s", source, dp->d_name);
      snprintf(dest_path, PATH_MAX, "%s/%s", dest, dp->d_name);
      execlp("mv", "mv", src_path, dest_path, NULL);
      exit(1);
    }
    else if (pid == -1)
    {
      perror("fork() failed");
      break;
    }
  }
  
  closedir(folder);
  
  // Wait for all child processes to complete
  while (wait(NULL) > 0);
}
```
Fungsi ``filter_animal`` merupakan fungsi yang memindahkan file-file gambar binatang dari sebuah direktori ke tiga direktori yang berbeda sesuai dengan kategori binatang tersebut, yaitu ``darat, amphibi, air``.

- Pertama, fungsi akan membuka sebuah direktori sumber dan mengecek setiap file yang ada di dalamnya. Setiap file yang ditemukan akan diperiksa nama filenya untuk menentukan kategori binatangnya, dengan cara mencari string ``darat, amphibi, atau air`` pada nama file. Jika nama file tidak mengandung kata kunci tersebut, maka fungsi akan mengabaikan file tersebut.
- Kedua, setelah kategori binatang dari sebuah file ditentukan, fungsi ini akan melakukan ``fork`` sebuah child process untuk memindahkan file tersebut ke directory tujuan yang sesuai dengan kategori binatangnya. Proses pemindahan dilakukan dengan perintah ``mv``.
- Ketiga, setelah pemindahan file telah selesai, fungsi ini menunggu sampai semua proses anak selesai dijalankan dengan memanggil fungsi ``wait``.

Berikut adalah implementasi dari fungsi ``main``:
```
filter_animal(".", "HewanDarat", "HewanAmphibi", "HewanAir");
```
Fungsi ``filter_animal`` dipanggil untuk memindahkan file-file gambar binatang ke direktori masing-masing berdasarkan kategori yang telah ditentukan. 

## Penjelasan fungsi ``zip_animal()``
Setelah selesai dipisah berdasarkan kategorinya masing-masing, folder-folder tersebut dizip kembali. Berikut adalah fungsi dari ``void zip_animal``:
```
/*
 * Compresses each directory of animal files into a ZIP archive using the `zip` command,
 * and then deletes the original directory.
 *
 * @param None
 * @return None
 */
void zip_animal() {
    // Define an array of directories to be compressed
    char *dirs[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    // Define an array of archive filenames to be created
    char *zips[] = {"HewanDarat.zip", "HewanAmphibi.zip", "HewanAir.zip"};

    // Loop through each directory and compress it into an archive
    for (int i = 0; i < 3; i++) {
        // Fork a child process to execute the `zip` command
        pid_t child_id = fork();
        if (child_id == 0) {
            // Execute the `zip` command with the appropriate arguments
            execlp("zip", "zip", "-r", zips[i], dirs[i], NULL);
            // Exit the child process
            exit(0);
        }
        // Wait for the child process to complete before continuing
        waitpid(child_id, NULL, 0);
        
        // Delete the directory after zipping
        // Open the directory using the `opendir` function
        DIR *dir = opendir(dirs[i]);
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            char path[1024];
            // Construct the full path to the directory entry
            sprintf(path, "%s/%s", dirs[i], entry->d_name);
            // Skip "." and ".." entries
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            // If the entry is a subdirectory, recursively delete it using `rmdir`
            if (entry->d_type == DT_DIR) {
                rmdir(path);
            } else {
                // If the entry is a file, delete it using `unlink`
                unlink(path);
            }
        }
        // Close the directory using the `closedir` function
        closedir(dir);
        // Delete the original directory using `rmdir`
        rmdir(dirs[i]);
    }
}
```
Fungsi tersebut melakukan kompresi file pada tiga direktori yang telah dibuat sebelumnya.
- Pertama, fungsi akan mendefinisikan dua array yaitu ``dirs`` yang berisi nama dari direktori yang akan dikompresi dan ``zips`` yang berisi nama arsip yang akan dibuat.
- Kedua, program akan melakukan iterasi pada masing-masing direktori yang terdapat pada array ``dirs``. Selanjutnya, program akan membuat proses anak dengan ``fork`` untuk mengeksekusi perintah ``zip`` dengan opsi ``-r``(rekursif) untuk mengkompresi isi direktori tersebut menjadi satu arsip dengan nama yang telah ditentukan pada array ``zips``.
- Ketiga, setelah selesai melakukan kompresi, program akan menghapus direktori asli dengan melakukan iterasi pada setiap file atau subdirektori yang ada di direktori tersebut dengan fungsi ``opendir, readdir, closedir, dan rmdir``. Jika ditemukan subidrektori, maka subidrektori tersebut akan dihapus secara rekursif dengan fungsi ``rmdir``. Jika ditemukan file, maka file tersebut akan dihapus dengan menggunakan fungsi ``unlink``. Terakhir, direktori asli akan dihapus menggunakan fungsi ``rmdir``.

Berikut adalah implementasi dari fungsi ``main``:
```
zip_animal();
```
Fungsi dipanggil untuk melakukan zip pada tiga direktori sesuai kategori, lalu menghapus direktori asli dengan tujuan untuk menghemat penyimpanan.


# Penjelasan soal nomor 2
```
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <signal.h>
 
// Fungsi untuk membuat program killer
void createKiller(int mode) {
    int status = 0;
    char* process_name = (mode == 1) ? "download_process" : "folder_process";
 
    // Generate c code for killer program
    FILE* fp = fopen("killer.c", "w+");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <unistd.h>\n\nint main() {\n");
    fprintf(fp, "\tsystem(\"pkill -f -e -c %s\");\n", process_name);
    fprintf(fp, "\treturn 0;\n}\n");
    fclose(fp);
 
    // Compile and remove c file
    pid_t killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    } else {
        waitpid(killer_pid, &status, 0);
    }

    pid_t rm_pid = fork();
    if(rm_pid == 0) {
        execl("/usr/bin/rm", "rm", "killer.c", NULL);
    } else {
        waitpid(rm_pid, &status, 0);
    }
}
 
// Fungsi utama
int main(int argc, char* argv[]) {
    // Check program mode
    if(argc != 2 || (strcmp("-a", argv[1]) != 0 && strcmp("-b", argv[1]) != 0)) {
        printf("PILIH MODE A dengan -a atau MODE B dengan -b\n");
        exit(1);
    }
    if (strcmp("-a",argv[1]) == 0) {
        printf("Mode A\n");
        createKiller(1);
    } else {
        printf("Mode B\n");
        createKiller(2);
    }

    // Pid list
    __pid_t pid;
    __pid_t folder_pid;
    __pid_t child_pid;
    __pid_t zip_pid;
 
    int zipStatus = 0;
 
    // Download link
    char link[100];
    time_t epoch;
    struct tm *cur_time;
 
    // Folder and file names
    char datetime[20];
    char foldertime[20];
    char outputFile[100];
    char *path = "//mnt/c/users/hasan/documents/sisop/mod2/no2rev";
 
    // Make folder creator into a daemon
    pid = fork(); // membuat process fork pertama
    if (pid < 0) {
        printf("Error in creating a daemon!\n");
        exit(1);
    }
    if (pid > 0) {
        printf("\n");
        exit(0);
    }




    umask(0); //// mengubah umask
    pid = setsid(); // Mengatur permission umask ke 0 dan membuat session baru dengan menggunakan setsid().

    // Folder downloaders
    while(1) {      //Melakukan loop tak terbatas dengan while(1), kemudian melakukan fork() dan membuat child process baru yang akan melakukan tugas tertentu.
        folder_pid = fork();
        sprintf(argv[0], "folder_process");
        if(folder_pid == 0) {

            time(&epoch);       //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel foldertime. Membuat direktori baru dengan menggunakan mkdir() dengan permission 0777.
            cur_time = localtime(&epoch);
            strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time);
            mkdir(foldertime, 0777);

            // Image downloaders
            for(int j = 0; j < 15; j++) { //Melakukan loop sebanyak 15 kali untuk membuat 15 child process baru yang akan mendownload gambar.
                child_pid = fork();
                sprintf(argv[0], "download_process");

                if(child_pid == 0) {        //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan chdir() untuk berpindah ke direktori yang ditentukan. Mengatur link URL yang akan didownload dan menyimpannya dalam variabel link. Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel datetime. Menggunakan sprintf() untuk mengatur format output file dan menyimpannya dalam variabel outputFile. Menggunakan execl() untuk menjalankan perintah wget untuk mendownload gambar. Setelah selesai, child process akan keluar dari program menggunakan exit(0).
                    time(&epoch);
                    cur_time = localtime(&epoch);
                    chdir(path);
                    sprintf(link, "https://p...content-available-to-author-only...m.photos/%ld", epoch%1000 + 50);
                    strftime(datetime, 100, "%Y-%m-%d_%X", cur_time);
                    sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime);
                    execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL);
                    exit(0);
                }
                sleep(5); //Menunda selama 5 detik menggunakan sleep().
            }
            // Melakukan fork() untuk membuat child process baru yang akan melakukan kompresi terhadap direktori yang telah dibuat sebelumnya. Menggunakan execl() untuk menjalankan perintah zip untuk melakukan kompresi. Setelah kompresi selesai, menggunakan waitpid() untuk menunggu child process selesai. Menggunakan execl() lagi untuk menjalankan perintah rm untuk menghapus direktori yang telah dikompresi. Setelah selesai, child process akan keluar dari program menggunakan exit(0).
            zip_pid = fork();
            if(zip_pid == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL);
            }   
            waitpid(zip_pid, &zipStatus, 0);
            execl("/usr/bin/rm", "rm -r ", foldertime, NULL);
            exit(0);
        }
        sleep(30); // Menunda selama 30 detik menggunakan sleep() 
    }
    return 0;
}
```

### Fungsi createKiller
```
void createKiller(int mode) {
    int status = 0;
    char* process_name = (mode == 1) ? "download_process" : "folder_process";
 
    // Generate c code for killer program
    FILE* fp = fopen("killer.c", "w+");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <unistd.h>\n\nint main() {\n");
    fprintf(fp, "\tsystem(\"pkill -f -e -c %s\");\n", process_name);
    fprintf(fp, "\treturn 0;\n}\n");
    fclose(fp);
 
    // Compile and remove c file
    pid_t killer_pid = fork();
    if(killer_pid == 0) {
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    } else {
        waitpid(killer_pid, &status, 0);
    }

    pid_t rm_pid = fork();
    if(rm_pid == 0) {
        execl("/usr/bin/rm", "rm", "killer.c", NULL);
    } else {
        waitpid(rm_pid, &status, 0);
    }
}
```
Fungsi createKiller(int mode) adalah sebuah fungsi dalam bahasa pemrograman C yang akan membuat sebuah program bernama killer yang dapat membunuh proses bernama download_process atau folder_process tergantung pada nilai parameter mode.

Pertama, fungsi ini akan memeriksa nilai mode dan menentukan nama proses yang akan dibunuh. Selanjutnya, fungsi ini akan membuat sebuah file killer.c dan menuliskan kode C di dalamnya yang akan menjalankan perintah pkill -f -e -c [nama proses] untuk membunuh proses tersebut.

Setelah kode C untuk program killer ditulis, fungsi ini akan melakukan kompilasi file killer.c menggunakan perintah gcc killer.c -o killer dan mengeksekusinya dengan execl(). Kemudian, fungsi ini juga akan menghapus file killer.c menggunakan perintah rm killer.c.

Dengan demikian, fungsi ini akan menghasilkan sebuah program bernama killer yang dapat membunuh proses download_process atau folder_process tergantung pada nilai parameter mode. Program ini akan otomatis dihapuskan setelah digunakan.

### Penjelasan Fungsi untuk Menjalankan
```
int main(int argc, char* argv[]) {
    // Check program mode
    if(argc != 2 || (strcmp("-a", argv[1]) != 0 && strcmp("-b", argv[1]) != 0)) {
        printf("PILIH MODE A dengan -a atau MODE B dengan -b\n");
        exit(1);
    }
    if (strcmp("-a",argv[1]) == 0) {
        printf("Mode A\n");
        createKiller(1);
    } else {
        printf("Mode B\n");
        createKiller(2);
    }
```
Fungsi ini adalah fungsi utama (main function) dari program yang akan dijalankan. Fungsi ini menerima dua parameter, yaitu argc dan argv, di mana argc adalah jumlah argumen yang diberikan saat menjalankan program, dan argv adalah array yang berisi argumen-argumen tersebut.

Pada awal fungsi, program akan mengecek apakah jumlah argumen yang diberikan adalah 2 dan apakah argumen tersebut adalah "-a" atau "-b". Jika tidak memenuhi kondisi tersebut, maka program akan menampilkan pesan kesalahan dan keluar dari program menggunakan fungsi exit().

Jika argumen yang diberikan adalah "-a", program akan menampilkan pesan "Mode A" dan memanggil fungsi createKiller(1) untuk membuat program "killer" yang akan digunakan untuk menghentikan proses "download_process".

Jika argumen yang diberikan adalah "-b", program akan menampilkan pesan "Mode B" dan memanggil fungsi createKiller(2) untuk membuat program "killer" yang akan digunakan untuk menghentikan proses "folder_process".

### Penjelasan pada fungsi awal dari proses daemon dan inisialisasi folder dan download
```
  // Pid list
    __pid_t pid;
    __pid_t folder_pid;
    __pid_t child_pid;
    __pid_t zip_pid;
 
    int zipStatus = 0;
 
    // Download link
    char link[100];
    time_t epoch;
    struct tm *cur_time;
 
    // Folder and file names
    char datetime[20];
    char foldertime[20];
    char outputFile[100];
    char *path = "//mnt/c/users/hasan/documents/sisop/mod2/no2rev";
 
    // Make folder creator into a daemon
    pid = fork(); // membuat process fork pertama
    if (pid < 0) {
        printf("Error in creating a daemon!\n");
        exit(1);
    }
    if (pid > 0) {
        printf("\n");
        exit(0);
    }
```
Code tersebut mendefinisikan beberapa variabel dan kemudian melakukan proses untuk membuat folder creator menjadi daemon. Beberapa penjelasan mengenai variabel-variabel yang didefinisikan adalah sebagai berikut:

pid, folder_pid, child_pid, dan zip_pid adalah variabel bertipe __pid_t yang masing-masing digunakan untuk menyimpan PID (Process ID) dari proses-proses yang akan dilakukan.
zipStatus adalah variabel bertipe int yang digunakan untuk menyimpan status dari proses zip.
link adalah array karakter dengan panjang 100 yang digunakan untuk menyimpan URL dari file yang akan didownload.
epoch adalah variabel bertipe time_t yang digunakan untuk menyimpan waktu saat ini dalam format epoch (jumlah detik sejak 1 Januari 1970).
cur_time adalah pointer ke struktur tm yang digunakan untuk menyimpan waktu saat ini dalam format tanggal dan waktu yang lebih mudah dibaca.
datetime, foldertime, dan outputFile adalah array karakter dengan masing-masing panjang 20, 20, dan 100, yang digunakan untuk menyimpan informasi mengenai folder dan file yang akan dibuat.
path adalah pointer ke string yang menyimpan path dari folder yang akan dibuat.
Setelah itu, dilakukan proses untuk membuat folder creator menjadi daemon dengan melakukan fork(), yaitu proses membuat salinan dari proses yang sudah ada. Jika proses fork() berhasil, maka pid akan menyimpan nilai 0 pada child process, dan nilai PID dari child process pada parent process. Jika proses fork() gagal, maka program akan keluar dengan pesan error.

Jika pid pada parent process bernilai lebih dari 0, maka program akan keluar dengan status 0. Dalam hal ini, parent process dijalankan dan selesai, sedangkan child process yang merupakan folder creator akan terus berjalan di background. Ini akan memungkinkan folder creator untuk menjalankan tugasnya tanpa harus diawasi secara langsung oleh pengguna.

### Penjelasan Fungsi download dan folder
```
umask(0); //// mengubah umask
    pid = setsid(); // Mengatur permission umask ke 0 dan membuat session baru dengan menggunakan setsid().

    // Folder downloaders
    while(1) {      //Melakukan loop tak terbatas dengan while(1), kemudian melakukan fork() dan membuat child process baru yang akan melakukan tugas tertentu.
        folder_pid = fork();
        sprintf(argv[0], "folder_process");
        if(folder_pid == 0) {

            time(&epoch);       //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel foldertime. Membuat direktori baru dengan menggunakan mkdir() dengan permission 0777.
            cur_time = localtime(&epoch);
            strftime(foldertime, 100, "%Y-%m-%d_%X", cur_time);
            mkdir(foldertime, 0777);

            // Image downloaders
            for(int j = 0; j < 15; j++) { //Melakukan loop sebanyak 15 kali untuk membuat 15 child process baru yang akan mendownload gambar.
                child_pid = fork();
                sprintf(argv[0], "download_process");

                if(child_pid == 0) {        //Mendapatkan waktu saat ini menggunakan time(&epoch) dan mengonversinya menjadi waktu lokal dengan localtime(&epoch). Menggunakan chdir() untuk berpindah ke direktori yang ditentukan. Mengatur link URL yang akan didownload dan menyimpannya dalam variabel link. Menggunakan strftime() untuk mengatur format waktu dan menyimpannya dalam variabel datetime. Menggunakan sprintf() untuk mengatur format output file dan menyimpannya dalam variabel outputFile. Menggunakan execl() untuk menjalankan perintah wget untuk mendownload gambar. Setelah selesai, child process akan keluar dari program menggunakan exit(0).
                    time(&epoch);
                    cur_time = localtime(&epoch);
                    chdir(path);
                    sprintf(link, "https://p...content-available-to-author-only...m.photos/%ld", epoch%1000 + 50);
                    strftime(datetime, 100, "%Y-%m-%d_%X", cur_time);
                    sprintf(outputFile, "--output-document=%s/%s.jpg", foldertime, datetime);
                    execl("/usr/bin/wget", "wget", "-q", outputFile, link, NULL);
                    exit(0);
                }
                sleep(5); //Menunda selama 5 detik menggunakan sleep().
            }
```
Code tersebut merupakan bagian dari program yang melakukan download gambar dari internet dan menyimpannya dalam folder yang baru dibuat dengan format nama sesuai waktu lokal saat itu. Potongan kode tersebut merupakan bagian dari loop tak terbatas yang akan membuat child process baru untuk mengunduh gambar secara berkala.

Pertama, pada baris pertama dilakukan pemanggilan fungsi umask(0) yang berfungsi untuk mengubah permission umask menjadi 0. Kemudian, pada baris kedua, dilakukan pemanggilan fungsi setsid() untuk membuat session baru dan mengatur permission umask ke 0.

Setelah itu, program akan masuk ke dalam loop tak terbatas dengan while(1). Di dalam loop tersebut, program akan melakukan fork dan membuat child process baru untuk melakukan tugas tertentu, yaitu membuat folder baru dengan format nama sesuai waktu lokal saat itu.

Child process tersebut akan menggunakan fungsi time() untuk mendapatkan waktu saat ini, kemudian menggunakan fungsi localtime() untuk mengubah waktu tersebut menjadi waktu lokal dan menyimpannya dalam variabel cur_time. Selanjutnya, fungsi strftime() digunakan untuk mengatur format waktu dan menyimpannya dalam variabel foldertime. Terakhir, fungsi mkdir() dipanggil untuk membuat folder baru dengan nama foldertime dan permission 0777.

Setelah folder baru berhasil dibuat, child process akan masuk ke dalam loop for dengan batas iterasi sebanyak 15 kali. Di dalam loop tersebut, child process akan melakukan fork lagi dan membuat child process baru untuk melakukan tugas tertentu, yaitu mendownload gambar dari internet.

Child process baru tersebut akan menggunakan fungsi time() untuk mendapatkan waktu saat ini, kemudian menggunakan fungsi localtime() untuk mengubah waktu tersebut menjadi waktu lokal dan menyimpannya dalam variabel cur_time. Kemudian, child process akan menggunakan fungsi chdir() untuk berpindah ke direktori yang ditentukan dalam variabel path.

Selanjutnya, child process akan mengatur link URL yang akan didownload dan menyimpannya dalam variabel link. Fungsi strftime() digunakan lagi untuk mengatur format waktu dan menyimpannya dalam variabel datetime. Terakhir, child process akan menggunakan fungsi sprintf() untuk mengatur format output file dan menyimpannya dalam variabel outputFile.

Setelah variabel link, datetime, dan outputFile berhasil diatur, child process akan menggunakan fungsi execl() untuk menjalankan perintah wget untuk mendownload gambar dari internet. Setelah selesai mendownload, child process akan keluar dari program menggunakan fungsi exit(0).

Sebelum keluar dari loop for, child process akan menunda selama 5 detik menggunakan fungsi sleep(). Setelah itu, child process akan kembali ke awal loop for untuk mengunduh gambar berikutnya.

### Penjelasan Fungsi ZIP
```
            zip_pid = fork();
            if(zip_pid == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldertime, foldertime, NULL);
            }   
            waitpid(zip_pid, &zipStatus, 0);
            execl("/usr/bin/rm", "rm -r ", foldertime, NULL);
            exit(0);
        }
        sleep(30); // Menunda selama 30 detik menggunakan sleep() 
    }
    return 0;
}
```
Code tersebut menjalankan proses untuk membuat file zip dari folder yang telah dibuat sebelumnya dan kemudian menghapus folder tersebut.

Proses dimulai dengan melakukan fork() untuk membuat child process baru yang akan melakukan tugas tersebut. Dalam child process, execl() digunakan untuk menjalankan perintah zip pada folder yang dibuat sebelumnya dengan menggunakan argumen "-r" untuk menghasilkan file zip rekursif. Setelah proses zip selesai, waitpid() digunakan untuk menunggu proses zip selesai dan menyimpan statusnya dalam variabel zipStatus.

Setelah proses zip selesai, execl() digunakan kembali untuk menjalankan perintah rm yang berfungsi untuk menghapus folder yang telah di-zip. Kemudian program keluar dari child process menggunakan exit(0).

Parent process akan menunggu selama 30 detik sebelum memulai proses berikutnya dengan menggunakan sleep(). Ini berarti proses tersebut akan berjalan setiap 30 detik hingga program dihentikan secara manual.


# Penjelasan soal nomor 3

Pada soal nomor 3, terdapat beberapa poin permasalahan yang perlu diselesaikan. Diantaranya sebagai berikut,
- **download** file zip dari url google drive yang disediakan (berisi gambar-gambar pemain sepak bola dari berbagai tim), kemudian **extract** file zip tersebut, dilanjut dengan **delete** file zip nya.
- Pada direktori players yang sudah di unzip, lakukan proses **delete** pada gambar pemain yang bukan dari tim Manchester United
- Lakukan proses **kategorisasi** pada pemain MU, buat folder Kiper, Bek, Gelandang, dan Penyerang, move masing-masing pemain ke dalam folder rolenya.
- **buat tim**, dengan mencari player dengan rating tertinggi di role-nya masing-masing.

## Fungsi Main
Pada fungsi main, akan dilakukan seluruh pemanggilan fungsi berdasarkan permasalahan pada soal. Sebagaimana berikut,
```
// poin A
downloadZip();
unzip();
remove("players.zip"); 

// poin B
chdir("players");
MU_Players(); 

// poin C
kategorisasi(); 

// poin D
buatTim(4, 4, 2);
```
Untuk selanjutnya, saya akan menjelaskan masing-masing fungsi yang dipanggil melalui main().
## Penjelasan fungsi downloadZip()
Sebagaimana pada nama fungsinya, pada fungsi ini akan dilakukan proses download zip dari url yang diberikan. Untuk melakukannya, disini saya lakukan proses forking. Nantinya, pada child process akan melibatkan fungsi execv untuk menjalankan argumen ``wget``, berikut potongan kodenya
```
if (child_id == 0){
  char *url = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
  char *argv[] = {"wget", "-q", "-O", "players.zip", url, NULL};
  execv("/bin/wget", argv);
} else {
  while ((wait(&status)) > 0);
  return;
}  
```
Sementara itu, bisa dilihat pada parent process hanya akan dilakukan proses wait, yaitu menunggu child process selesai download baru return ke main. Untuk tambahan, pada array char argv, saya berikan tambahan -q untuk men-*silent* output dari wget.

## Penjelasan fungsi unzip()
Pada fungsi ini akan dilakukan proses unzip dari zip yang telah didownload melalui fungsi sebelumnya, dengan cara yang kurang lebih sama yaitu melibatkan penggunaan forking dan fungsi execv untuk menjalankan argumen ``unzip``, berikut potongan kodenya
```
if (child_id == 0){
  char *argv[] = {"unzip", "-qq", "players.zip", NULL};
  execv("/bin/unzip", argv);
} else {
  while ((wait(&status)) > 0);
  return;
}

```
Tambahan: setelah dilakukan proses unzip, pada fungsi main akan dipanggil fungsi bawaan C yaitu ``remove("players.zip")`` untuk menghapus zip tersebut.

## Penjelasan fungsi MU_Players()
Sebelum masuk ke penjelasan, sebelum dipanggilnya fungsi ``MU_Players()`` saya akan pindah terlebih dahulu ke directory players (berisikan gambar-gambar pemain) dengan menggunakan command chdir().
```
chdir("players");
```
Karena directory aktif sekarang sudah ada di players, maka aman untuk dilakukan pemanggilan fungsi ``MU_Players()``, yaitu fungsi untuk menghapus seluruh player yang bukan dari Manchester United.

Pada fungsi ini, saya menggunakan library ``dirent`` untuk melakukan traverse terhadap nama file-file gambar yang ada di folder players. Pertama, kita akan membuka directory aktif sebagaimana pada potongan kode berikut,
```
dir = opendir(".");
if (dir == NULL){
    exit(EXIT_FAILURE);
}
```
Kedua, kita akan men-iterasi seluruh nama file di directory dan mengecek nama file mana yang mengandung kata kunci "ManUtd" melalui fungsi while berikut
```
while (fgets(buffer, sizeof(buffer), fp)){
  if (strstr(entry->d_name, "ManUtd") != NULL)
  {
      contains_word = 1;
      //printf("%s\n", entry->d_name);
      break;
  }
} 
```
Dapat dilihat saya menggunakan command strstr untuk mengecek nama entry / file yang sedang diiterasi, jika true maka value ``contains_word`` saya ubah menjadi 1. Nantinya, value ``contains_word`` akan diperiksa, jika bernilai 0 / false, maka hapus file yang sedang diiterasi tersebut sebagaimana berikut,
```
if (!contains_word){
  remove(entry->d_name);
}
```
Pada akhirnya, directory akan diclose dan fungsi akan kembali ke main().

## Penjelasan fungsi kategorisasi
Fungsi kategorisasi() bertujuan untuk membuat 4 folder baru yaitu Kiper, Bek, Gelandang, dan Striker. Selain itu, juga akan dilakukan proses move seluruh gambar pemain ke dalam folder role nya masing-masing. Sehingga, disini saya lakukan proses forking dimana proses child akan melakukan pembuatan folder dan parent proses akan melakukan proses move.
Berikut potongan kode di child proses,
```
if (child_id == 0){
  char *programName = "mkdir";
  char *arg = "Kiper";
  char *arg1 = "Bek";
  char *arg2 = "Gelandang";
  char *arg3 = "Penyerang";
  execlp(programName, programName, "-p", arg, arg1, arg2, arg3, NULL);
}
```
Disini saya menggunakan command ``execlp`` untuk pemanggilan fungsi mkdir diikuti argumen lainnya yaitu nama-nama folder yang akan dibuat.

Sementara itu, pada parent process akan dilakukan open active directory dilanjut dengan traverse dan pengecekan ke seluruh nama file gambar yang ada.
```
while (fgets(buffer, sizeof(buffer), fp)){ // menghindari segmentation fault
  if (strstr(entry->d_name, "Kiper") != NULL)
  {
    mvToFolder(entry->d_name, "Kiper");
    break;
  }
  else if (strstr(entry->d_name, "Bek") != NULL)
  {
    mvToFolder(entry->d_name, "Bek");
    break;
  }
  else if (strstr(entry->d_name, "Gelandang") != NULL)
  {
    mvToFolder(entry->d_name, "Gelandang");
    break;
  }
  else if (strstr(entry->d_name, "Penyerang") != NULL)
  {
    mvToFolder(entry->d_name, "Penyerang");
    break;
  }
}
```
Jika nama file mengandung kata kunci kiper/bek/gelandang/penyerang maka panggil fungsi mvToFolder dengan passing nama file serta string "role" nya. berikut potongan kode di fungsi ``mvToFolder(argfile, argdest)`` yang juga menggunakan metode forking() untuk memanggil argumen ``mv``.
```
if (child_id == 0){
  char *programName = "mv";
  execlp(programName, programName, argfile, argdest, NULL);
} else {
  while ((wait(&status)) > 0);
  return;
}
```
Bisa dilihat bahwa argfile adalah nama-file yang ingin dipindah dan argdest adalah tujuan foldernya.
## Penjelasan fungsi buatTim()
Pada fungsi ``buatTim(int bek, int gelandang, int penyerang)``, akan dilakukan pemilihan terhadap pemain MU berdasarkan rating tertinggi di masing-masing rolenya, jumlah player ditentukan dari apa yang di passing di fungsi. Nantinya nama pemain tersebut akan ditulis ke dalam file .txt yang diletakkan di /home/user. Berikut potongan kode untuk format penamaan txt nya,
```
char format[50];
snprintf(format, 50, "formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
chdir("/home/ahda"); // ganti nama user anda sendiri
```
Saya juga menggunakan forking pada fungsi ini, dimana child proses akan melakukan proses touch file txt dengan nama format. Karena directory aktif sudah diganti ke home/user, maka langsung saja kita lakukan execv pada child process sebagaimana berikut.
```
if (child_id == 0){
  char *argv[] = {"touch", format, NULL};
  execv("/bin/touch", argv);
}
```
Sementara itu, untuk parent process akan dilakukan proses writing ke dalam file txt yang sudah dibuat.
Pertama, kita open file tersebut menggunakan command fopen() dilanjut dengan mengganti active directory kembali ke players
```
FILE *fp = fopen(format, "w");        
chdir("sisop/modul2/players/"); // ganti ke tempat dimana folder players berada
```
Selanjutnya, terdapat 4 for-loop dengan masing-masing iterasi dilakukan sebanyak pemain yang diinginkan. 
```
fprintf(fp, "%s\n", "Formasi Bek:");
for (int i = 0; i < bek; i++){
    fprintf(fp, "%s\n", filter("Bek"));
}

fprintf(fp, "\n%s\n", "Formasi Gelandang:");
for (int i = 0; i < gelandang; i++){
    fprintf(fp, "%s\n", filter("Gelandang"));
}

fprintf(fp, "\n%s\n", "Formasi Penyerang:");
for (int i = 0; i < penyerang; i++){
    fprintf(fp, "%s\n", filter("Penyerang"));
}

fprintf(fp, "\n%s", "Kiper: ");
for (int i = 0; i < 1; i++){
    fprintf(fp, "%s", filter("Kiper"));
}
```
Bisa dilihat, untuk melakukan penulisan nama pemain ke file txt, digunakan command fprintf() ke file fp (file format.txt). Dengan string yang ditulis adalah hasil return dari fungsi filter dan parameter string "role" nya.

Berikut penjelasan fungsi dari filter()

## Penjelasan fungsi filter(role)
Intinya, pada fungsi ini akan dilakukan traversing terhadap nama-nama file gambar pemain seperti fungsi-fungsi sebelumnya. Tetapi, yang kita perlukan dari nama kali ini adalah rating pemainnya, dengan cara berikut
```
char *last = strrchr(entry->d_name, '_');
if (last != NULL) {
  int rating = atoi(last+1);
  if (rating > maxRating) maxRating = rating;
}
```
Melibatkan command sttrchr untuk mengambil seluruh karakter mulai dari '_' yang muncul terakhir, yaitu nilai rating pemain. Kemudian, kita akan konversi string tersebut menjadi integer melalui fungsi bawaan atoi()
Setelah itu, kita akan konversi max rating saat ini menjadi string kembali dengan tujuan untuk traverse nama pemain kembali, dan ambil pemain yang memiliki rating tersebut dengan melibatkan command strstr() sebagaimana berikut,
```
if (strstr(cek->d_name, rating) != NULL)
{
    chosen = cek->d_name;
    mvToFolder(chosen, "/home/ahda/sisop/modul2/players"); // temporary pindah player yang dipilih 
    chdir(".."); // balik ke folder players, untuk persiapan role selanjutnya
    return chosen; // pemain dengan rating tertinggi
}
```
Bisa dilihat kita akan menyimpan nama pemain pada variabel chosen, lalu kita akan temporarily memindah file pemain tersebut ke folder ``players``dengan tujuan untuk menghindari pemain duplikat, lalu kita akan mundur 1 directory untuk persiapan role selanjutnya dan return string chosen (nama pemain yang terpilih).

# Penjelasan soal nomor 4
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, char *argv[]) {
    // Check if the correct number of arguments is provided
    if (argc != 5) {
        printf("[Error]: Wrong amount of argument\n");
        return 1;
    }

    // Convert arguments to integers or -1 if "*" is used as argument
    int hour = strcmp(argv[1], "*") == 0 ? -1 : atoi(argv[1]);
    int minute = strcmp(argv[2], "*") == 0 ? -1 : atoi(argv[2]);
    int second = strcmp(argv[3], "*") == 0 ? -1 : atoi(argv[3]);

    // Check if the provided time is valid
    if (hour < -1 || hour > 23 || minute < -1 || minute > 59 || second < -1 || second > 59) {
        printf("[Error]: Time is not valid\n");
        return 1;
    }

    // Get the filename from the command line arguments
    char *filename = argv[4];

    // Check if the file exists and can be accessed
    if (access(filename, F_OK) == -1) {
        printf("[Error]: File is not found\n");
        return 1;
    }

    // Create a new process
    pid_t pid = fork();

    // Check if the fork was successful
    if (pid == -1) {
        printf("[Error]: Fork failed\n");
        return 1;
    }

    if (pid == 0) {
        // child process

        // Loop indefinitely
        while (1) {
            // Get the current time
            time_t now = time(NULL);
            struct tm *timeinfo = localtime(&now);

            // Check if the current time matches the specified time
            if ((hour == -1 || hour == timeinfo->tm_hour) &&
                (minute == -1 || minute == timeinfo->tm_min) &&
                (second == -1 || second == timeinfo->tm_sec)) {
                // If the time matches, execute the specified file
                execlp("bash", "bash", filename, NULL);
            }

            // Sleep for one second before checking again
            sleep(1);
        }
    } else {
        // parent process
        return 0;
    }
}
```
## Penjelasan header yang digunakan
Ada beberapa header file yang digunakan untuk mengakses berbagai fungsi dari sistem operasi dan fungsi bawaan C. Berikut penjelasan singkat dari setiap header file tersebut:
- ``stdio.h``: header file ini mengandung fungsi standar untuk input dan output, seperti ``printf()`` dan ``scanf()``.
- ``stdlib.h``: header file ini mengandung fungsi standar untuk alokasi memori, konversi numerik, pengurutan, dan fungsi lain yang berkaitan dengan pengelolaan memori.
- ``unistd.h``: header file ini mengandung konstanta, tipe data, dan fungsi untuk mengakses fungsi sistem (system calls), seperti ``fork(), exec(), dan sleep()``.
- ``string.h``: header file ini mengandung fungsi standar untuk manipulasi string, seperti ``strcpy(), strlen(), dan strcmp()``.
- ``sys/types.h``: header file ini mengandung definisi tipe data seperti ``pid_t, off_t, dan size_t`` yang digunakan oleh sistem operasi.
- ``sys/stat.h``: header file ini mengandung definisi tipe data dan konstanta yang digunakan untuk mengakses informasi tentang file dan direktori.
- ``fcntl.h``: header file ini mengandung definisi tipe data dan konstanta yang digunakan untuk mengontrol akses ke file dan direktori.
- ``time.h``: header file ini mengandung fungsi standar untuk manipulasi waktu dan tanggal, seperti ``time(), localtime(), dan strftime()``.

## Penjelasan Program
Program dimulai dengan fungsi ``main`` yang diawali dengan pengecekan jumlah argumen yang diberikan saat program dijalankan.

```
int main(int argc, char *argv[]) {
    // Check if the correct number of arguments is provided
    if (argc != 5) {
        printf("[Error]: Wrong amount of argument\n");
        return 1;
    }
```
Jika jumlah argumen kurang atau lebih dari 5, maka program akan mencetak pesan error dan mengembalikan nilai ``1``. Jadi, program tersebut memerlukan 5 argumen saat dijalankan.

Selanjutnya, kode berikut mengubah argumen string yang diberikan di baris perintah menjadi nilai integer. Jika argumen adalah ``"*" ``(bintang), maka variabel integer yang sesuai akan diset menjadi -1. Ini karena karakter ``"*"`` digunakan dalam program ini untuk menunjukkan bahwa tidak ada batasan waktu tertentu pada jam, menit, atau detik tertentu yang harus dipertimbangkan saat menjalankan program.
```
// Convert arguments to integers or -1 if "*" is used as argument
    int hour = strcmp(argv[1], "*") == 0 ? -1 : atoi(argv[1]);
    int minute = strcmp(argv[2], "*") == 0 ? -1 : atoi(argv[2]);
    int second = strcmp(argv[3], "*") == 0 ? -1 : atoi(argv[3]);

```
Operator ternary digunakan untuk mengatur nilai integer sesuai dengan argumen yang diberikan. Jika argumen adalah ``"*"`` maka variabel integer akan diset menjadi -1, sedangkan jika argumen adalah bilangan bulat, variabel integer akan diisi dengan nilai yang sesuai. Fungsi strcmp() digunakan untuk membandingkan string dan mengecek apakah argumen yang diberikan adalah ``"*"``. Fungsi atoi() digunakan untuk mengubah argumen string menjadi bilangan bulat.

Kode berikut melakukan pengecekan apakah waktu yang diberikan sebagai argumen ke program valid atau tidak. Waktu yang dianggap valid adalah waktu dengan rentang jam antara 0 - 23, menit antara 0 - 59, dan detik antara 0 - 59.
```
// Check if the provided time is valid
    if (hour < -1 || hour > 23 || minute < -1 || minute > 59 || second < -1 || second > 59) {
        printf("[Error]: Time is not valid\n");
        return 1;
    }
```
Jika waktu yang diberikan tidak valid, program akan menampilkan pesan kesalahan ``"Time is not valid"`` dan mengembalikan nilai ``1`` untuk menandakan bahwa program berakhir dengan kesalahan.

Pada kode berikut argumen keempat diambil dari baris perintah dan menyimpannya sebagai string ``filename``. Kemudian, program memeriksa apakah file yang ditentukan dalam argumen keempat ada dan dapat diakses dengan fungsi ``access``.
```
// Get the filename from the command line arguments
    char *filename = argv[4];

    // Check if the file exists and can be accessed
    if (access(filename, F_OK) == -1) {
        printf("[Error]: File is not found\n");
        return 1;
    }
```
 Fungsi ``access`` memeriksa apakah file yang ditentukan dapat dibaca, ditulis, atau diakses oleh proses yang sedang berjalan. Jika ``access`` mengembalikan nilai ``-1``, artinya file tidak ditemukan atau tidak dapat diakses, program mencetak pesan kesalahan dan keluar dengan kode status ``1``.

 Langkah terakhir, program akan membuat proses baru dengan menggunakan fungsi ``fork()``. Jika proses fork berhasil, maka akan ada dua proses yang berjalan secara parallel yaitu parent process dan child process.
```
 // Create a new process
    pid_t pid = fork();

    // Check if the fork was successful
    if (pid == -1) {
        printf("[Error]: Fork failed\n");
        return 1;
    }

    if (pid == 0) {
        // child process

        // Loop indefinitely
        while (1) {
            // Get the current time
            time_t now = time(NULL);
            struct tm *timeinfo = localtime(&now);

            // Check if the current time matches the specified time
            if ((hour == -1 || hour == timeinfo->tm_hour) &&
                (minute == -1 || minute == timeinfo->tm_min) &&
                (second == -1 || second == timeinfo->tm_sec)) {
                // If the time matches, execute the specified file
                execlp("bash", "bash", filename, NULL);
            }

            // Sleep for one second before checking again
            sleep(1);
        }
    } else {
        // parent process
        return 0;
    }
}

```
Kemudian program akan melakukan pengecekan dengan condition statement untuk menentukan apakah ``fork`` berhasil atau tidak dengan memeriksa apakah nilai dari ``pid`` sama dengan ``-1``. Jika ya, maka program akan mencetak pesan error dan mengembalikan nilai ``1``(tidak sukses).

Jika proses fork berhasil, maka program akan mengeksekusi kondisi if yang merupakan child process yang akan berjalan secara terus menerus dan akan melakukan pengecekan waktu setiap satu detik menggunakan fungsi ``sleep(1)``. Program akan mengecek apakah waktu saat ini sudah sesuai dengan waktu yang dimasukkan pada argumen atau tidak.

Jika waktu sesuai dengan waktu yang dimasukkan, maka program akan mengeksekusi file bash yang dimasukkan pada argumen terakhir dengan menggunakan fungsi ``execlp()``. Fungsi ``execlp()`` akan menggantikan image proses saat ini dengan image proses baru.

Jika proses fork bukan child process, maka program akan mengembalikan nilai ``0`` dan berakhir.