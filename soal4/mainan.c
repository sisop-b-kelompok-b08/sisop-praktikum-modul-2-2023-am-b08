#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, char *argv[]) {
    // Check if the correct number of arguments is provided
    if (argc != 5) {
        printf("[Error]: Wrong amount of argument\n");
        return 1;
    }

    // Convert arguments to integers or -1 if "*" is used as argument
    int hour = strcmp(argv[1], "*") == 0 ? -1 : atoi(argv[1]);
    int minute = strcmp(argv[2], "*") == 0 ? -1 : atoi(argv[2]);
    int second = strcmp(argv[3], "*") == 0 ? -1 : atoi(argv[3]);

    // Check if the provided time is valid
    if (hour < -1 || hour > 23 || minute < -1 || minute > 59 || second < -1 || second > 59) {
        printf("[Error]: Time is not valid\n");
        return 1;
    }

    // Get the filename from the command line arguments
    char *filename = argv[4];

    // Check if the file exists and can be accessed
    if (access(filename, F_OK) == -1) {
        printf("[Error]: File is not found\n");
        return 1;
    }

    // Create a new process
    pid_t pid = fork();

    // Check if the fork was successful
    if (pid == -1) {
        printf("[Error]: Fork failed\n");
        return 1;
    }

    if (pid == 0) {
        // child process

        // Loop indefinitely
        while (1) {
            // Get the current time
            time_t now = time(NULL);
            struct tm *timeinfo = localtime(&now);

            // Check if the current time matches the specified time
            if ((hour == -1 || hour == timeinfo->tm_hour) &&
                (minute == -1 || minute == timeinfo->tm_min) &&
                (second == -1 || second == timeinfo->tm_sec)) {
                // If the time matches, execute the specified file
                execlp("bash", "bash", filename, NULL);
            }

            // Sleep for one second before checking again
            sleep(1);
        }
    } else {
        // parent process
        return 0;
    }
}