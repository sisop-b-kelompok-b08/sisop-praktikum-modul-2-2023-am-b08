#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <regex.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

/**
 * Downloads a file from the given URL and saves it as the given file name using the 'wget' utility.
 *
 * @param url - The URL of the file to be downloaded.
 * @param file_animal - The name of the file to be saved.
 */
void download_file(char* url, char* file_animal) {

  // Fork a new process to run 'wget' utility in the child process.
  pid_t pid = fork();

  // Check if the current process is the child process.
  if (pid == 0) {
    
    // In the child process, execute the 'wget' command with the given arguments.
    execlp("wget", "wget", "--no-check-certificate", url, "-q", "-O", file_animal, (char*) NULL);
    
    // Exit the child process if the 'execlp' command fails.
    exit(1);
  }

  // Wait for the child process to complete.
  int status;
  waitpid(pid, &status, 0);
}


/**
 * Unzips a given ZIP file using the 'unzip' utility.
 *
 * @param animal_zip - The name of the ZIP file to be unzipped.
 */
void unzip_animal(char* animal_zip) {

  // Fork a new process to run 'unzip' utility in the child process.
  pid_t pid = fork();

  // Check if the current process is the child process.
  if (pid == 0) {
    
    // In the child process, execute the 'unzip' command with the given arguments.
    execlp("unzip", "unzip", "-q", animal_zip, (char*) NULL);
    
    // Exit the child process if the 'execlp' command fails.
    exit(1);
  }

  // Wait for the child process to complete.
  int status;
  waitpid(pid, &status, 0);
}

/**
 * Selects a random image file in the given directory with the extension '.jpg', 
 * and writes its filename to a new text file named 'penjaga.txt'.
 *
 * @param directory - The path to the directory containing the image files.
 */
void get_random_file(char *directory)
{
  // Open the directory for reading.
  DIR *folder = opendir(directory);
  if (!folder) return;

  // Read the filenames of image files in the directory and store them in the 'path' array.
  struct dirent *dp;
  char path[100][256];  // Array to store filenames
  int count = 0;

  while ((dp = readdir(folder)) && count < 100)
  {
    if (dp->d_type == DT_REG && strstr(dp->d_name, ".jpg"))
      strcpy(path[count++], dp->d_name);
  }

  // Close the directory.
  closedir(folder);

  // If no image files were found in the directory, return without creating the text file.
  if (!count) return;

  // Generate a random number within the range of the 'path' array to select a random image file.
  srand(time(NULL));
  int random = rand() % count;

  // Create a new text file 'penjaga.txt' and write the name of the selected image file to it.
  FILE *file = fopen("penjaga.txt", "w");
  if (!file) return;

  fprintf(file, "Hewan yang dijaga : %.*s", (int)(strrchr(path[random], '.') - path[random]), path[random]);
  fclose(file);
}


/**
 * Creates three directories named HewanDarat, HewanAir, and HewanAmphibi if they do not exist yet.
 * Uses fork() to create child processes for each directory creation to run in parallel.
 */
void make_directory() {
  // Array of directory names to create
  const char* dirs[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
  pid_t child_id;
  int stat;
  // Loop through each directory name and create a child process for each using fork()
  for (int i = 0; i < sizeof(dirs) / sizeof(*dirs); i++) {
    if ((child_id = fork()) == 0) {
      // Child process calls mkdir command with -p flag to create the directory and its parent directories if they do not exist
      execlp("mkdir", "mkdir", "-p", dirs[i], NULL);
      // Exit child process
      exit(0);
    }
    // Wait for child process to finish before moving to the next directory
    waitpid(child_id, &stat, 0);
  }
}



/**
 * This function filters animal images into three different categories
 * based on the string "darat", "air", or "amphibi" in their filename,
 * and moves them to the corresponding directories.
 *
 * @param source The source directory containing the animal images
 * @param dest1 The destination directory for animals categorized as "darat"
 * @param dest2 The destination directory for animals categorized as "air"
 * @param dest3 The destination directory for animals categorized as "amphibi"
 */
void filter_animal(char *source, char *dest1, char *dest2, char *dest3)
{
  struct dirent *dp;
  DIR *folder;
  pid_t pid;
  
  // Open the source directory
  folder = opendir(source);
  if (folder == NULL)
    return;
  
  // Traverse all files in the source directory
  while ((dp = readdir(folder)) != NULL)
  {
    // Ignore non-regular files
    if (dp->d_type != DT_REG)
      continue;
      
    char *dest = NULL;
    // Determine the category of the animal image
    if (strstr(dp->d_name, "darat") != NULL)
      dest = dest1;
    else if (strstr(dp->d_name, "amphibi") != NULL)
      dest = dest2;
    else if (strstr(dp->d_name, "air") != NULL)
      dest = dest3;
    else
      continue;
      
    // Fork a child process to move the file to the corresponding directory
    pid = fork();
    if (pid == 0)
    {
      char src_path[PATH_MAX];
      char dest_path[PATH_MAX];
      snprintf(src_path, PATH_MAX, "%s/%s", source, dp->d_name);
      snprintf(dest_path, PATH_MAX, "%s/%s", dest, dp->d_name);
      execlp("mv", "mv", src_path, dest_path, NULL);
      exit(1);
    }
    else if (pid == -1)
    {
      perror("fork() failed");
      break;
    }
  }
  
  closedir(folder);
  
  // Wait for all child processes to complete
  while (wait(NULL) > 0);
}


/*
 * Compresses each directory of animal files into a ZIP archive using the `zip` command,
 * and then deletes the original directory.
 *
 * @param None
 * @return None
 */
void zip_animal() {
    // Define an array of directories to be compressed
    char *dirs[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    // Define an array of archive filenames to be created
    char *zips[] = {"HewanDarat.zip", "HewanAmphibi.zip", "HewanAir.zip"};

    // Loop through each directory and compress it into an archive
    for (int i = 0; i < 3; i++) {
        // Fork a child process to execute the `zip` command
        pid_t child_id = fork();
        if (child_id == 0) {
            // Execute the `zip` command with the appropriate arguments
            execlp("zip", "zip", "-r", zips[i], dirs[i], NULL);
            // Exit the child process
            exit(0);
        }
        // Wait for the child process to complete before continuing
        waitpid(child_id, NULL, 0);
        
        // Delete the directory after zipping
        // Open the directory using the `opendir` function
        DIR *dir = opendir(dirs[i]);
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            char path[1024];
            // Construct the full path to the directory entry
            sprintf(path, "%s/%s", dirs[i], entry->d_name);
            // Skip "." and ".." entries
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            // If the entry is a subdirectory, recursively delete it using `rmdir`
            if (entry->d_type == DT_DIR) {
                rmdir(path);
            } else {
                // If the entry is a file, delete it using `unlink`
                unlink(path);
            }
        }
        // Close the directory using the `closedir` function
        closedir(dir);
        // Delete the original directory using `rmdir`
        rmdir(dirs[i]);
    }
}


/*
 * Main function to download, unzip, filter, and zip animal image files
 */
int main(){

    /*
     * Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. 
     * Dan untuk melakukan melihat file gambar pada folder yang telah didownload 
     * Grape-kun harus melakukan unzip pada folder tersebut
     */

    // Download animal image files from Google Drive
    download_file("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");

    // Unzip animal image files to 'binatang' directory
    unzip_animal("binatang.zip");

    /*
     * Setelah berhasil melakukan unzip 
     * Grape-kun melakukan pemilihan secara acak pada file gambar tersebut 
     * untuk melakukan shift penjagaan pada hewan tersebut
     */

    // Select a random animal image file from the 'binatang' directory
    get_random_file(".");

    /*
     * Karena Grape-kun adalah orang yang perfeksionis 
     * Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. 
     * Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. 
     * Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan 
     * sesuai dengan tempat tinggal nya.
     */

    // Create directories 'HewanDarat', 'HewanAmphibi', 'HewanAir' to store animal images
    make_directory();

    // Move animal images to their respective directories based on their habitat
    filter_animal(".", "HewanDarat", "HewanAmphibi", "HewanAir");

    /*
     * Setelah mengetahui hewan apa saja yang harus dijaga 
     * Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan
     */

    // Zip directories 'HewanDarat', 'HewanAmphibi', 'HewanAir' to save space
    zip_animal();

    return 0;
}
