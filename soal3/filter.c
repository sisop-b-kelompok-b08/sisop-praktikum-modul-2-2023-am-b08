#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <zlib.h> // buat chdir()
#include <wait.h>
#include <dirent.h>

// gcc filter.c -o filter 
void downloadZip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *url = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
        char *argv[] = {"wget", "-q", "-O", "players.zip", url, NULL};
        execv("/bin/wget", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }  
}

void unzip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", "-qq", "players.zip", NULL};
        execv("/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }
}

void MU_Players(){
    DIR *dir;
    struct dirent *entry;

    dir = opendir(".");
    if (dir == NULL){
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL){
        
        int contains_word = 0;
        char buffer[256];
        FILE *fp = fopen(entry->d_name, "r");

        while (fgets(buffer, sizeof(buffer), fp)){
            if (strstr(entry->d_name, "ManUtd") != NULL)
            {
                contains_word = 1;
                //printf("%s\n", entry->d_name);
                break;
            }
        }
        fclose(fp);
        if (!contains_word){
            remove(entry->d_name);
        }
    }

    closedir(dir);
}

void mvToFolder(char *argfile, char *argdest){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *programName = "mv";
        execlp(programName, programName, argfile, argdest, NULL);
    } else {
        while ((wait(&status)) > 0);
        return;
    }
    
}

void kategorisasi(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *programName = "mkdir";
        char *arg = "Kiper";
        char *arg1 = "Bek";
        char *arg2 = "Gelandang";
        char *arg3 = "Penyerang";
        execlp(programName, programName, "-p", arg, arg1, arg2, arg3, NULL);
    } else {
        
        while ((wait(&status)) > 0);
    
        DIR *dir;
        struct dirent *entry;

        dir = opendir(".");
        if (dir == NULL){
            exit(EXIT_FAILURE);
        }

        while ((entry = readdir(dir)) != NULL){
            
            char buffer[256];
            FILE *fp = fopen(entry->d_name, "r");

            while (fgets(buffer, sizeof(buffer), fp)){ // menghindari segmentation fault
                if (strstr(entry->d_name, "Kiper") != NULL)
                {
                    mvToFolder(entry->d_name, "Kiper");
                    break;
                }
                else if (strstr(entry->d_name, "Bek") != NULL)
                {
                    mvToFolder(entry->d_name, "Bek");
                    break;
                }
                else if (strstr(entry->d_name, "Gelandang") != NULL)
                {
                    mvToFolder(entry->d_name, "Gelandang");
                    break;
                }
                else if (strstr(entry->d_name, "Penyerang") != NULL)
                {
                    mvToFolder(entry->d_name, "Penyerang");
                    break;
                }
            }
            fclose(fp);
        }
        closedir(dir);  
    }
    
}

const char* filter(char role[]){
   
    chdir(role); 

    DIR *dir;
    struct dirent *entry, *cek;

    dir = opendir(".");
    if (dir == NULL){
        exit(EXIT_FAILURE);
    }

    int maxRating = 0;

    while ((entry = readdir(dir)) != NULL){
   
        char *last = strrchr(entry->d_name, '_');
        if (last != NULL) {
            int rating = atoi(last+1);
            if (rating > maxRating) maxRating = rating;
        }
    }
    
    char rating[2];
    sprintf(rating, "%d", maxRating);
    char *chosen;

    dir = opendir(".");
    while ((cek = readdir(dir)) != NULL){
        
        if (strstr(cek->d_name, rating) != NULL)
        {
            chosen = cek->d_name;
            mvToFolder(chosen, "/home/ahda/sisop/modul2/players"); // temporary pindah player yang dipilih 
            chdir(".."); // balik ke folder players, untuk persiapan role selanjutnya
            return chosen; // pemain dengan rating tertinggi
        }
    }

}

void buatTim(int bek, int gelandang, int penyerang){

    pid_t child_id;
    child_id = fork();
    int status;
    char format[50];
    snprintf(format, 50, "formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    chdir("/home/ahda"); // ganti nama user anda sendiri

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"touch", format, NULL};
        execv("/bin/touch", argv);
    } else {
        while ((wait(&status)) > 0);
        
        FILE *fp = fopen(format, "w");
        
        chdir("sisop/modul2/players/"); // ganti ke tempat dimana folder players berada

        fprintf(fp, "%s\n", "Formasi Bek:");
        for (int i = 0; i < bek; i++){
            fprintf(fp, "%s\n", filter("Bek"));
        }
        
        fprintf(fp, "\n%s\n", "Formasi Gelandang:");
        for (int i = 0; i < gelandang; i++){
            fprintf(fp, "%s\n", filter("Gelandang"));
        }
        
        fprintf(fp, "\n%s\n", "Formasi Penyerang:");
        for (int i = 0; i < penyerang; i++){
            fprintf(fp, "%s\n", filter("Penyerang"));
        }

        fprintf(fp, "\n%s", "Kiper: ");
        for (int i = 0; i < 1; i++){
            fprintf(fp, "%s", filter("Kiper"));
        }
        
    }
}

int main(){
    // poin A
    downloadZip();
    unzip();
    remove("players.zip"); 

    // poin B
    chdir("players");
    MU_Players(); 
    
    // poin C
    kategorisasi(); 

    // poin D
    buatTim(4, 4, 2);

    // kategorisasi ulang, karena pemain yang dipilih dikeluarkan dari folder
    kategorisasi();
}

//char s[100];
//printf("%s\n", getcwd(s, 100));